package model.dao;

//Estructura de datos
import java.util.ArrayList;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

//Clase para conexión
import util.JDBCUtilities;

//Encapsulamiento de los datos
import model.vo.Lider;

public class LiderDao {

    public ArrayList<Lider> query_requerimiento_4() throws SQLException {
        Connection conexion = JDBCUtilities.getConnection();
        // Crea arreglo para almacenar objetos tipo Proyecto
        ArrayList<Lider> lideres = new ArrayList<Lider>();

        try (Statement estado = conexion.createStatement();
                ResultSet rest = estado.executeQuery("Select Nombre, Primer_Apellido from Lider l inner join Proyecto p on l.ID_Lider = p.ID_Lider where p.Constructora = 'Pegaso'");) {

            while (rest.next()) {
                lideres.add(new Lider(rest.getString("Nombre"), rest.getString("Primer_Apellido")));
            }
        }

        return lideres;
    }// Fin del método query_requerimiento_4

}
