package model.dao;

//Estructura de datos
import java.util.ArrayList;

import model.vo.Lider;
import model.vo.Proyecto;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

//Clase para conexión
import util.JDBCUtilities;

public class ProyectoDao {

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {

        ArrayList<Proyecto> pro = new ArrayList<Proyecto>();

        try (Statement estado = JDBCUtilities.getConnection().createStatement();
                ResultSet rest = estado.executeQuery("Select Fecha_Inicio, Numero_Habitaciones, Numero_Banos from proyecto where Constructora = 'Pegaso'");) {

            while (rest.next()) {

                pro.add(new Proyecto(rest.getString("Fecha_Inicio"), rest.getInt("Numero_Habitaciones"), rest.getInt("Numero_Banos"), "", 0));

            }
        }

        JDBCUtilities.getConnection().close();

        return pro;

    }

    public ArrayList<Proyecto> query_requerimiento_2() throws SQLException {

        ArrayList<Proyecto> pro = new ArrayList<Proyecto>();

        try (Statement estado = JDBCUtilities.getConnection().createStatement();
                ResultSet rest = estado.executeQuery("Select Fecha_Inicio,Numero_Habitaciones,Numero_Banos, Nombre, Primer_Apellido, Estrato "
                        + "from Proyecto p join Lider l on p.ID_Lider = l.ID_Lider join Tipo t on t.ID_Tipo = p.ID_Tipo where p.Constructora = 'Pegaso'")) {

            while (rest.next()) {
                pro.add(new Proyecto(rest.getString("Fecha_Inicio"), rest.getInt("Numero_Habitaciones"),
                        rest.getInt("Numero_Banos"), "", rest.getInt("Estrato"),
                        new Lider(rest.getString("Nombre"), rest.getString("Primer_Apellido"))));
            }
        }

        //JDBCUtilities.getConnection().close();
        return pro;

    }// Fin del método query_requerimiento_2

    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {

        ArrayList<Proyecto> pro = new ArrayList<Proyecto>();
        try (Statement estado = JDBCUtilities.getConnection().createStatement();
                ResultSet rest = estado.executeQuery("Select sum(Numero_Habitaciones) as total, Constructora from proyecto group by Constructora")) {

            while (rest.next()) {

                pro.add(new Proyecto(rest.getInt("total"), rest.getString("Constructora")));

            }
        }

        //JDBCUtilities.getConnection().close();
        return pro;
    }// Fin del método query_requerimiento_3

    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException {

        ArrayList<Proyecto> pro = new ArrayList<Proyecto>();

        try (Statement estado = JDBCUtilities.getConnection().createStatement();
                ResultSet rest = estado.executeQuery("Select sum(Numero_Habitaciones) as total, Constructora from Proyecto "
                        + "group by Constructora having sum(Numero_Habitaciones) > 200.0 order by sum(Numero_Habitaciones) ASC");) {

            while (rest.next()) {
                pro.add(new Proyecto(rest.getInt("total"), rest.getString("Constructora")));
            }
        }
        return pro;

    }// Fin del método query_requerimiento_4

}
